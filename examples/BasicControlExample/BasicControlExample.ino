//////////////////////////////////////////////////////////////////////////////
//Description: Example basic control code using a MCP2515 CAN Bus Interface module. This code uses
//             the CAN_BUS_SHIELD library found here:
//             https://github.com/Seeed-Studio/CAN_BUS_Shield
//             The motor controller must have the most recent firmware and be set to
//             some non-zero ID number.
//
// Motor is controlled by the following commands over serial:
//    - ' ' = STOP
//    - 'r X.YY' = Set percent output to value in [-1.0, 1.0].
//
/////////////////////////////////////////////////////////////////////////////

#include <Arduino.h>
#include <SPI.h>
#include "SparkMax.h"
#include "TalonSrx.h"

#define MTR_CAN_ID 5 // ID of the motor controller we are communicating with

// The motor we are controlling
//! Uncomment the motor type with which you are interfacing.
SparkMax motor(MTR_CAN_ID);
//TalonSrx motor(MTR_CAN_ID);

//============SPI CAN Module============
#include <mcp_can.h>

#include "can-serial.h"
#include "mcp2515_can.h"
#include "mcp_can.h"

// CAN Bus pins
#ifdef ESP32
    #define CAN_INT_PIN 9
    #define CAN_CS_PIN 5
#else
    #define CAN_INT_PIN 9
    #define CAN_CS_PIN 8
#endif

mcp2515_can CAN(CAN_CS_PIN);

// Initialization function for the CAN module.
bool canInit() {
    //! Note: The CAN MCP2515 CAN module must be using a 16MHz oscilator or THIS WILL NOT WORK!
    return CAN.begin(CAN_1000KBPS, MCP_16MHz) == CAN_OK;   
}

// Sending function for the CAN module
void canSend(unsigned long id, uint8_t ext, uint8_t rtrBit, uint8_t len, const uint8_t *dat) {
    CAN.sendMsgBuf(id, ext, len, dat);
}

// Receiving function for the CAN module
unsigned canReceive(uint32_t *id, uint8_t *dat) {
    byte len = 0;
    CAN.readMsgBuf(&len, dat);
    *id = CAN.getCanId();
    return len;
}

//=====setup================================================================
void setup() {
    Serial.begin(115200);
    Serial.setTimeout(5000);
    while (!Serial)
        ;  // wait for Serial

    Serial.println("initializing...");
    Serial.flush();

    // Initialize the CAN module
    if (!canInit()) {
        Serial.println("CAN initialization failed.");
        while (true) {
            delay(1000);
        }
    }
    Serial.println("CAN BUS Shield Init OK!");

    // Add the sender for the CAN communications.
    Serial.println("Adding CAN sender...");
    FrcMotorController::addCanSender(canSend);

    // Clear faults    
    Serial.println("Clearing faults...");
    motor.clearFaults();

    Serial.println("Starting main loop.");
}

//=====loop================================================================
void loop() {
    static unsigned long lastPrint = 0;
    char readVal = 0;
    static bool running = false;
    // Update at a fixe interval (<100ms I think)
    delay(80);
    FrcMotorController::sendKeepAlive();
    
    // Check if any serial data available
    while (Serial.available()) {
        readVal = Serial.read();
    }

    // Control the selected motor (If given one)
    static float val = 0.0;
    switch (readVal) {
    case ' ': // STOP
        motor.setPower(0.0);
        running = false;
        Serial.println("[stop]");
        break;
    case 'r': // Percent output control
        val = Serial.parseFloat(SKIP_WHITESPACE);
        motor.setPower(val);
        running = true;
        Serial.print("[percent]:");
        Serial.println(val);
        break;
    default:
        break;
    }

    // Note on some weirdness: CTR motor controllers must be continuously sent the value for it to keep running. This is *not* the case on the Spark Max. So the below code is not needed in that case. 
    static unsigned long lastSend = millis();
    if (running && millis() - lastSend > 50) {
        motor.setPower(val);
        lastSend = millis();
    }
}
