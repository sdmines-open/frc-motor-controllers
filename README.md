# FRC Motor Controllers Arduino Library  
This repo contains an Arduino library for interfacing with various motor controllers commonly used in FRC.  

Motor controllers supported:  
 - REV SPARK MAX  
 - CTR Talon SRX  
 - CTR Victor SPX  