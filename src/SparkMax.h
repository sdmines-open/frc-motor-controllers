#ifndef __SPARK_MAX_H__
#define __SPARK_MAX_H__

#include <Arduino.h>

#include "FrcMotorController.h"

class SparkMax : public FrcMotorController
{
public:
    enum class MotorType
    {
        kBrushed = 0,
        kBrushless = 1
    };

    enum class ConfigParams
    {
        PAR_CanID = 0,
        PAR_InputMode = 1,
        PAR_MotorType = 2,
        PAR_CommAdvance = 3,
        PAR_SensorType = 4,
        PAR_CtrlType = 5,
        PAR_IdleMode = 6,
        PAR_InputDeadband = 7,
        PAR_FeedbackSensorPID0 = 8,
        PAR_FeedbackSensorPID1 = 9,
        PAR_PolePairs = 10,
        PAR_CurrentChop = 11,
        PAR_CurrentChopCycles = 12,
        PAR_P_0 = 13,
        PAR_I_0 = 14,
        PAR_D_0 = 15,
        PAR_F_0 = 16,
        PAR_IZone_0 = 17,
        PAR_DFilter_0 = 18,
        PAR_OutputMin_0 = 19,
        PAR_OutputMax_0 = 20,
        PAR_P_1 = 21,
        PAR_I_1 = 22,
        PAR_D_1 = 23,
        PAR_F_1 = 24,
        PAR_IZone_1 = 25,
        PAR_DFilter_1 = 26,
        PAR_OutputMin_1 = 27,
        PAR_OutputMax_1 = 28,
        PAR_P_2 = 29,
        PAR_I_2 = 30,
        PAR_D_2 = 31,
        PAR_F_2 = 32,
        PAR_IZone_2 = 33,
        PAR_DFilter_2 = 34,
        PAR_OutputMin_2 = 35,
        PAR_OutputMax_2 = 36,
        PAR_P_3 = 37,
        PAR_I_3 = 38,
        PAR_D_3 = 39,
        PAR_F_3 = 40,
        PAR_IZone_3 = 41,
        PAR_DFilter_3 = 42,
        PAR_OutputMin_3 = 43,
        PAR_OutputMax_3 = 44,
        PAR_Inverted = 45,
        PAR_OutputRatio = 46,
        PAR_SerialNumberLow = 47,
        PAR_SerialNumberMid = 48,
        PAR_SerialNumberHigh = 49,
        PAR_LimitSwitchFwdPolarity = 50,
        PAR_LimitSwitchRevPolarity = 51,
        PAR_HardLimitFwdEn = 52,
        PAR_HardLimitRevEn = 53,
        PAR_SoftLimitFwdEn = 54,
        PAR_SoftLimitRevEn = 55,
        PAR_RampRate = 56,
        PAR_FollowerID = 57,
        PAR_FollowerConfig = 58,
        PAR_SmartCurrentStallLimit = 59,
        PAR_SmartCurrentFreeLimit = 60,
        PAR_SmartCurrentConfig = 61,
        PAR_SmartCurrentReserved = 62,
        PAR_MotorKv = 63,
        PAR_MotorR = 64,
        PAR_MotorL = 65,
        PAR_MotorRsvd1 = 66,
        PAR_MotorRsvd2 = 67,
        PAR_MotorRsvd3 = 68,
        PAR_EncoderCountsPerRev = 69,
        PAR_EncoderAverageDepth = 70,
        PAR_EncoderSampleDelta = 71,
        PAR_EncoderInverted = 72,
        PAR_EncoderRsvd1 = 73,
        PAR_VoltageCompMode = 74,
        PAR_CompensatedNominalVoltage = 75,
        PAR_SmartMotionMaxVelocity_0 = 76,
        PAR_SmartMotionMaxAccel_0 = 77,
        PAR_SmartMotionMinVelOutput_0 = 78,
        PAR_SmartMotionAllowedClosedLoopError_0 = 79,
        PAR_SmartMotionAccelStrategy_0 = 80,
        PAR_SmartMotionMaxVelocity_1 = 81,
        PAR_SmartMotionMaxAccel_1 = 82,
        PAR_SmartMotionMinVelOutput_1 = 83,
        PAR_SmartMotionAllowedClosedLoopError_1 = 84,
        PAR_SmartMotionAccelStrategy_1 = 85,
        PAR_SmartMotionMaxVelocity_2 = 86,
        PAR_SmartMotionMaxAccel_2 = 87,
        PAR_SmartMotionMinVelOutput_2 = 88,
        PAR_SmartMotionAllowedClosedLoopError_2 = 89,
        PAR_SmartMotionAccelStrategy_2 = 90,
        PAR_SmartMotionMaxVelocity_3 = 91,
        PAR_SmartMotionMaxAccel_3 = 92,
        PAR_SmartMotionMinVelOutput_3 = 93,
        PAR_SmartMotionAllowedClosedLoopError_3 = 94,
        PAR_SmartMotionAccelStrategy_3 = 95,
        PAR_IMaxAccum_0 = 96,
        PAR_Slot3Placeholder1_0 = 97,
        PAR_Slot3Placeholder2_0 = 98,
        PAR_Slot3Placeholder3_0 = 99,
        PAR_IMaxAccum_1 = 100,
        PAR_Slot3Placeholder1_1 = 101,
        PAR_Slot3Placeholder2_1 = 102,
        PAR_Slot3Placeholder3_1 = 103,
        PAR_IMaxAccum_2 = 104,
        PAR_Slot3Placeholder1_2 = 105,
        PAR_Slot3Placeholder2_2 = 106,
        PAR_Slot3Placeholder3_2 = 107,
        PAR_IMaxAccum_3 = 108,
        PAR_Slot3Placeholder1_3 = 109,
        PAR_Slot3Placeholder2_3 = 110,
        PAR_Slot3Placeholder3_3 = 111,
        PAR_PositionConversionFactor = 112,
        PAR_VelocityConversionFactor = 113,
        PAR_ClosedLoopRampRate = 114,
        PAR_SoftLimitFwd = 115,
        PAR_SoftLimitRev = 116,
        PAR_SoftLimitRsvd0 = 117,
        PAR_SoftLimitRsvd1 = 118,
        PAR_AnalogRevPerVolt = 119,
        PAR_AnalogRotationsPerVoltSec = 120,
        PAR_AnalogAverageDepth = 121,
        PAR_AnalogSensorMode = 122,
        PAR_AnalogInverted = 123,
        PAR_AnalogSampleDelta = 124,
        PAR_AnalogRsvd0 = 125,
        PAR_AnalogRsvd1 = 126,
        PAR_DataPortConfig = 127,
        PAR_AltEncoderCountsPerRev = 128,
        PAR_AltEncoderAverageDepth = 129,
        PAR_AltEncoderSampleDelta = 130,
        PAR_AltEncoderInverted = 131,
        PAR_AltEncodePositionFactor = 132,
        PAR_AltEncoderVelocityFactor = 133,
        PAR_numParameters
    };

    SparkMax(uint8_t deviceId);

    void setParameter(ConfigParams param, bool value);
    void setParameter(ConfigParams param, uint16_t value);
    void setParameter(ConfigParams param, float value);

    void setInverted(bool invert);
    void setIdleMode(IdleMode mode);

    void setPower(float power);
    void setVelocity(float velocity);
    void setPosition(float position);
    void setPidSlot(uint8_t slot);

    void enableVoltageCompensation(double nominalVoltage);
    void disableVoltageCompensation();
    float getAppliedOutput();
    float getBusVoltage();
    float getOutputCurrent();
    float getTemperature();
    float getPosition();
    float getVelocity();
    bool limitForward();
    bool limitReverse();
    void restoreFactoryDefaults(bool persist = false);
    void burnFlash();

    void clearFaults();

protected:
    static void handleKeepAlive();

    fptr getKeepAliveHandler() { return SparkMax::handleKeepAlive; }

private:
    typedef struct PACKED
    {
        int16_t appliedOutput;  // 16-bit signed integer representing the applied output
        uint16_t faults;
        uint16_t stickyFaults;
        uint8_t sensorInv : 1;
        uint8_t setpointInv : 1;
        uint8_t lock : 2;
        uint8_t mtrType : 1;
        uint8_t isFollower : 1;
        uint8_t roboRIO : 1;
        uint8_t rsvd0 : 1;
    } PeriodicStatus0;

    typedef struct __attribute__((__packed__))
    {
        float sensorVel;           // 32-bit float representing the motor velocity in RPM
        uint8_t mtrTemp;           // Motor temperature in Celcius
        uint16_t mtrVoltage : 12;  // 12-bit fixed point value representing the motor voltage
        uint16_t mtrCurrent : 12;
    } PeriodicStatus1;

    typedef struct __attribute__((__packed__))
    {
        float sensorPos;  // Motor position in rotations
        int32_t iAccum;
    } PeriodicStatus2;

    typedef struct __attribute__((__packed__))
    {
        uint32_t analogVoltage : 10;
        int32_t analogVel : 22;
        int32_t analogPos;
    } PeriodicStatus3;

    typedef struct __attribute__((__packed__))
    {
        float altEncoderVelocity;
        float altEncoderPosition;
    } PeriodicStatus4;

    // Track the most recently received status messages
    PeriodicStatus0 lastStat0;
    PeriodicStatus1 lastStat1;
    PeriodicStatus2 lastStat2;

    uint8_t pidSlot = 0;            // PID config used for closed loop control. 0-3.
    static uint16_t activeDevices;  // Data value for the keep-alive message

    bool handleIncoming(unsigned msgType, byte* dat, unsigned len);
    void handleSetParameter(ConfigParams param, uint32_t value);
};

#endif